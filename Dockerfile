# vim:set ft=dockerfile:
FROM alpine:3.13.7

COPY .ruby-version .ruby-version

RUN \
  apk update \
  && \
  apk add --no-cache \
  curl \
  bash \
  git \
  jq \
  alpine-sdk \
  build-base \
  openssl \
  tar \
  gcc \
  libc-dev \
  make \
  linux-headers \
  openssl-dev \
  zlib-dev \
  && \
  git clone https://github.com/rbenv/ruby-build.git \
  && \
  PREFIX=/usr/local ./ruby-build/install.sh \
  && \
  ruby-build $(cat .ruby-version) /usr/local \
  && \
  apk del --no-cache glib-dev linux-headers openssl-dev zlib-dev \
  && \
  gem install --no-document json \
  && \
  gem install --no-document yaml-lint \
  && \
  gem install --no-document bundler \
  && \
  gem cleanup \
  && \
  rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

ENTRYPOINT ["/bin/sh", "-c"]
