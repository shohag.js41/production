#! /usr/bin/env bash

set -euo pipefail

set -x

dry_run="${DRY_RUN:-1}"

if [ "${dry_run}" == '0' ]; then
    echo 'Creating extension:'
    gitlab-psql --command='CREATE EXTENSION pageinspect;'
else
    echo "[Dry-run] Would have ran command: gitlab-psql --command='CREATE EXTENSION pageinspect;'"
fi
