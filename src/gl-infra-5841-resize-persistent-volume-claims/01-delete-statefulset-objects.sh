#! /usr/bin/env bash

set -euo pipefail

set -x

# Iteratively delete the statefulset objects:
for i in $(seq 0 14); do
    echo "Deleting statefulset object: sts/thanos-store-$i"

    read -p "Continue? (y/N)" -n 1 -r
    echo
    [[ $REPLY =~ ^[Yy]$ ]] || break

    # Delete the statefulset object without deleting the pods underneath:
    kubectl --namespace=monitoring delete --cascade=orphan sts/thanos-store-$i

    echo "Waiting for statefulset object deletion..."
    kubectl --namespace=monitoring wait --for=delete sts/thanos-store-$i

    # Confirm that thanos-store pods still exist:
    kubectl --namespace=monitoring get pod --selector=app.kubernetes.io/name=thanos-store
done
