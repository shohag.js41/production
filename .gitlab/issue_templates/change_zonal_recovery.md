# Production Change - Criticality 2 ~C2 <!-- In production this should be a C1 since there will be a down time as well as block deployments but in staging this is a C2-->

## Change Summary

This prodcution issue is to be used in the event of a zonal outage and we are required to restore Gitaly VMs in a single zone.

### Change Details

1. **Services Impacted**  - ~"Service::Gitaly"
1. **Change Technician**  - <!-- woodhouse: '`@{{ .Username }}`' -->{+ DRI for the execution of this change +}
1. **Change Reviewer**    - <!-- woodhouse: '@{{ .Reviewer }}' -->{+ DRI for the review of this change +}
1. **Time tracking**      - <!-- woodhouse: '{{ .Duration}}' --> 60 minutes
1. **Downtime Component** - <!-- woodhouse: '{{ .Downtime }}'-->{+ Include downtime estimate here +}

{+ Provide a brief summary indicating the affected zone +}
<!-- e.g Restoring `gitaly-01` in `us-east1-b` as `gitaly-01a` in `us-east1-c` due to the outage in `us-east1-b` related incident/issue -->

## Preparation Tasks

1. [ ] Ensure the MRs to provision Patroni replicas and the Gitaly VMs are rebased and approved.
    * MR that adds Patroni replicas with zone overrides. https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/6922
    * MR that adds Gitaly servers within the available zone. https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/6923
1. [ ] Notify the release managers on _Slack_ by mentioning `@release-managers` and referencing this issue and await their acknowledgment.
1. [ ] Notify the eoc on _Slack_ by mentioning `@sre-oncall` and referencing this issue and wait for approval by adding the ~eoc_approved label.
    * **Example message:** _`@release-managers` or `@sre-oncall` LINK_TO_THIS_CR is scheduled for execution. This should not affect deployments, however to avoid dataloss we will be forcing downtime, that will likely last (TBD after execution of [Gameday in gstg](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17189))._

## Detailed steps for the change

### Change Steps - steps to take to execute the change

#### Execution

1. [ ] Set label ~"change::in-progress" `/label ~change::in-progress`
1. [ ] Merge the MR that adds a new node with a zone override to the override list.
   * https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/6922
1. [ ] Merge the MR to add a new Gitaly server within the available zone.
   * https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/6923

  **NOTE:** *These two steps above :arrow_up: take a while to complete the other steps can be executed in parallel*

1. [ ] Drain the corresponding zonal Kubernetes cluster using set-server-state for the failed zone, and evaluate recovery of the other zonal clusters.
    * `chef-repo$ ./bin/set-server-state -n gstg -z <zone {b,c,d}> maint`
1. [ ] Reconfigure the regional cluster to exclude the affected zone by setting `regional_cluster_zones` in Terraform to a list of zones that are not impacted ([example MR](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/7161)).
1. [ ] Drain the canary environment by running the following command in the Slack [#production](https://gitlab.slack.com/archives/C101F3796) channel:
     * `/chatops run canary --disable --staging`
     * Ref [GitLab Chatops](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/canary.md#how-to-stop-all-production-traffic-to-canary)

##### Validation of the Patroni replica and Gitaly VMs

* [ ] Tail the serial output to confirm that the start up script executed successfully.
  * `gcloud compute --project=gitlab-staging-1 instances tail-serial-port-output patroni-main-v14-105-db-gstg --zone=us-east1-c --port=1`
  * `gcloud compute --project=gitlab-staging-1 instances tail-serial-port-output patroni-ci-v14-105-db-gstg --zone=us-east1-c --port=1`
  * `gcloud compute --project=gitlab-staging-1 instances tail-serial-port-output patroni-registry-v14-4-db-gstg --zone=us-east1-c --port=1`
  * `gcloud compute --project=gitlab-gitaly-gstg-380a instances tail-serial-port-output gitaly-01a-stor-gstg --zone=us-east1-c --port=1`
  * `gcloud compute --project=gitlab-gitaly-gstg-164c instances tail-serial-port-output gitaly-01a-stor-gstg --zone=us-east1-c --port=1`
  **NOTE:** _The hostnames above are examples remember to replace them_

* [ ] Wait for the instances to be built and for Chef to run
  * Staging: [Confirm that chef runs have completed](https://thanos-query.ops.gitlab.net/graph?g0.expr=max(chef_client_last_run_timestamp_seconds%7Benv%3D%22gstg%22%2C%20type%3D\~%22gitaly%7Cpatroni%7Cpatroni-ci%7Cpatroni-registry%22%7D)%20by%20(fqdn)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) for new storages and patroni replicas.
  * We could also tail bootstrap logs example: `tail -f /var/tmp/bootstrap-20231108-133642.log`
* [ ] Once the patroni replicas are ready ssh into each replica and start patroni:
  * `sudo systemctl enable patroni && systemctl start patroni`
* [ ] ssh into the Gitaly VMs:
  * Execute `sudo gitlab-ctl status` to validate that the servers are up
  * Execute `df -h` to validate that the disk is properly mounted.

## **POINT OF NO RETURN**

### Add the new Storages to the Rails and Gitaly configuration

1. [ ] Create an MR against chef-repo that adds the new storages to the environment Chef managed configuration. Note it will take around 30 minutes for Chef to converge.
    * Staging example: https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/4074
1. [ ] Create an MR against k8s-workloads/gitlab-com that will add the new storage to the K8s configuration
    * Staging example: https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/3142

#### Wrapping up

1. [ ] Re-enable canary
   * [ ] In the [#production](https://gitlab.slack.com/archives/C101F3796) Slack channel to re-enable the canary fleet run the command `/chatops run canary --enable --staging`
   * Set server state for `gitaly-01` back to ready.
     * [ ] `chef-repo$ ./bin/set-server-state -n gstg -z <zone {b,c,d}> ready`
1. [ ] Set label ~"change::complete" `/label ~change::complete`

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

#### *It is estimated that this will take 5m to complete*

1. [ ] Re-enable canary
   * [ ] In the [#production](https://gitlab.slack.com/archives/C101F3796) Slack channel to re-enable the canary fleet run the command `/chatops run canary --enable --staging`
   * Set server state for `gitaly-01` back to ready.
     * [ ] `chef-repo$ ./bin/set-server-state -n gstg -z <zone {b,c,d}> ready`
2. [ ] Set label ~"change::aborted" `/label ~change::aborted`

## Monitoring

### Key metrics to observe

* Completed Chef runs: [Staging](https://thanos-query.ops.gitlab.net/graph?g0.expr=max(chef_client_last_run_timestamp_seconds%7Benv%3D%22gstg%22%2C%20type%3D%22gitaly%22%7D)%20by%20(fqdn)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
* Gitaly Dashboard: [Staging](https://dashboards.gitlab.net/d/gitaly-main/gitaly-overview?from=now-6h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&orgId=1)
* Gitaly RPS by FQDN: [Staging](https://thanos.gitlab.net/graph?g0.expr=sum%20by%20(fqdn)%20(%0A%20%20rate(gitaly_service_client_requests_total%7Benv%3D%22gstg%22%2Cenvironment%3D%22gstg%22%2Cfqdn!%3D%22%22%2Cjob%3D%22gitaly%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%7D%5B1m%5D)%0A)%0A%20%3E%200&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
* Gitaly Errors by FQDN: [Staging](https://thanos.gitlab.net/graph?g0.expr=sum%20by%20(fqdn)%20(%0A%20%20label_replace(rate(gitaly_service_client_requests_total%7Benv%3D%22gstg%22%2Cenvironment%3D%22gstg%22%2Cfqdn!%3D%22%22%2Cgrpc_code!~%22AlreadyExists%7CCanceled%7CDeadlineExceeded%7CFailedPrecondition%7CInvalidArgument%7CNotFound%7COK%7CPermissionDenied%7CResourceExhausted%7CUnauthenticated%22%2Cjob%3D%22gitaly%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%7D%5B1m%5D)%2C%20%22_c%22%2C%20%220%22%2C%20%22%22%2C%20%22%22)%0A%20%20or%0A%20%20label_replace(rate(gitaly_service_client_requests_total%7Bdeadline_type!%3D%22limited%22%2Cenv%3D%22gstg%22%2Cenvironment%3D%22gstg%22%2Cfqdn!%3D%22%22%2Cgrpc_code%3D%22DeadlineExceeded%22%2Cjob%3D%22gitaly%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%7D%5B1m%5D)%2C%20%22_c%22%2C%20%221%22%2C%20%22%22%2C%20%22%22)%0A)%0A%20%3E%200&g0.tab=0&g0.stacked=0&g0.range_input=6h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
* Patroni service: [Staging](https://dashboards.gitlab.net/goto/S8D8Kz7SR?orgId=1)
* PostgreSQL Overview: [Staging](https://dashboards.gitlab.net/goto/pNYqFk7SR?orgId=1)
* Replication Lag: [Staging](https://thanos.gitlab.net/graph?g0.expr=pg_replication_lag%7Benv%3D%22gstg%22%2Cfqdn%3D~%22patroni-main-v14-.*%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=2h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

/label ~"C2"
/label ~"change::unscheduled"
/label ~"change"
